<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::post('/iniciar-sesion','AutentificacionController@iniciarSesion');
Route::post('/logout','AutentificacionController@logout');

Route::resource('/cliente','ClienteController');

Route::get('/factura', function (){
 return view('factura');
});
Route::get('/producto', function (){
    return view('producto');
});
