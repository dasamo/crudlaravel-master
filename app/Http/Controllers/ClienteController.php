<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;
use App\Detalle;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cliente = Cliente::all();
        $detalle = Detalle::all();
        return view('index', compact('cliente','detalle'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $detalle = Detalle::all();
        return view('formulario',compact('detalle'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cliente = new Cliente();   
        $cliente->nombre = $request->txtnombre;
        $cliente->tipo_documento = $request->txttipo;
        $cliente->identificacion = $request->txtdocumento;
        $cliente->telefono = $request->txttelefono;
        $cliente->direccion = $request->txtdireccion;
        $cliente->correo= $request->txtcorreo;
        $cliente->estado= 1;
        $cliente->save();
       return redirect()->route('cliente.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cliente = Cliente::findorfail($id);
        $detalle = Detalle::all();
        return view('edit', compact('cliente','detalle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cliente = Cliente::find($id);
        $cliente->nombre = $request->txtnombre;
        $cliente->tipo_documento = $request->txttipo;
        $cliente->identificacion = $request->txtdocumento;
        $cliente->telefono = $request->txttelefono;
        $cliente->direccion = $request->txtdireccion;
        $cliente->correo= $request->txtcorreo;
        $cliente->estado= 1;
        $cliente->save();
        return redirect()->route('cliente.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $cliente = Cliente::find($id);
            $cliente->delete();
            return redirect()->route('cliente.index');
        }catch (Exception $e){
            return "fatal error - ".$e->getMessage();
        }
    }
}
