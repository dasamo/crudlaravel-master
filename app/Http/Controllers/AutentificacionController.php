<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\LoginFormRequest;

class AutentificacionController extends Controller
{
    public function iniciarSesion(LoginFormRequest $request){
        //Login personalizado
        if (Auth::attempt(['email' => $request->email, 'password' => $request->clave], false)) {
            return response()->json('has iniciado sesion',200);
            
        }else{
            return response()->json(['errors'=> ['login' => ['Los datos que ingresaste son incorectos']]],422);
        }
    }
    public function logout(){
        Auth::logout();
    }
}
