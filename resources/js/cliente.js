import Axios from "axios";

new Vue({
    el: '#app_cliente',

    methods: {
        logout: function(){
            Axios.post('/logout', {
            }).then( response=>{
                swal({
                    title: 'Has cerrado sesion',
                    text: 'Hasta la proxima',
                    icon: 'success',
                    closeOnClickOutside: false,
                    closeOnEsc: false
                }).then( select => {
                    if( select ){
                        window.location.href = '/' 
                    }
                });
            });
        }
    },
});
