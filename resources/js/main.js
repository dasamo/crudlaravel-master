import Axios from "axios";

//codigo personalizado de javascript
new Vue({
    el: '#app_login',
    data: {
        usuario: '',
        clave: ''
    },
    methods: {
        
        iniciarSesion: function(){
            Axios.post('/iniciar-sesion', {
                email: this.usuario,
                clave: this.clave
            })
            .then( response=>{
                swal({
                    title: 'Has iniciado sesion',
                    text: 'Datos correctos',
                    icon: 'success',
                    closeOnClickOutside: false,
                    closeOnEsc: false
                }).then( select => {
                    if( select ){
                        window.location.href = 'cliente' 
                    }
                });
                console.log(this.usuario, this.clave);
            })
            .catch(error=>{
                let err = error.response.data.errors;
                let mensaje = "Error no identificado";
                if(err.hasOwnProperty('email')){
                    mensaje = err.email[0];
                }else if(err.hasOwnProperty('clave')){
                    mensaje = err.clave[0];
                }else if(err.hasOwnProperty('login')){
                    mensaje = err.login[0];
                }

                swal('Error', mensaje, 'error');
            });
            
        }
    },
});
