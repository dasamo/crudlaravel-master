<html>
<!-- begin::Head -->

<head>
    <meta charset="utf-8" />
    <title>Avícola|Factura</title>
    <meta name="description" content="Blank inner page examples">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->

    <!--begin::Global Theme Styles -->
    <link href="/assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
    <link href="/assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Global Theme Styles -->
    <!--begin::Page Vendors Styles -->
    <link href="/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Page Vendors Styles -->
    <link rel="shortcut icon" href="../../public/assets/demo/default/media/img/logo/favicon.ico" />
</head>

<!-- end::Head -->

<!-- begin::Body -->
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled
    m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <!-- BEGIN: Header -->
    <header id="m_header" class="m-grid__item    m-header " m-minimize-offset="200" m-minimize-mobile-offset="200">
        <div class="m-container m-container--fluid m-container--full-height">
            <div class="m-stack m-stack--ver m-stack--desktop">

                <!-- BEGIN: Brand -->
                <div class="m-stack__item m-brand  m-brand--skin-dark ">
                    <div class="m-stack m-stack--ver m-stack--general">
                        <div class="m-stack__item m-stack__item--middle m-brand__logo">
                            <a href="index.html" class="m-brand__logo-wrapper">
                                <img alt="" src="/assets/imagenes/logo-empresa.png" />
                            </a>
                        </div>
                        <div class="m-stack__item m-stack__item--middle m-brand__tools">

                            <!-- BEGIN: Left Aside Minimize Toggle -->
                            <a href="javascript:;" id="m_aside_left_minimize_toggle"
                               class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block  ">
                                <span></span>
                            </a>

                            <!-- END -->

                            <!-- BEGIN: Responsive Aside Left Menu Toggler -->
                            <a href="javascript:;" id="m_aside_left_offcanvas_toggle"
                               class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                                <span></span>
                            </a>

                            <!-- END -->

                            <!-- BEGIN: Responsive Header Menu Toggler -->
                            <a id="m_aside_header_menu_mobile_toggle" href="javascript:;"
                               class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
                                <span></span>
                            </a>

                            <!-- END -->

                            <!-- BEGIN: Topbar Toggler -->
                            <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;"
                               class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                                <i class="flaticon-more"></i>
                            </a>

                            <!-- BEGIN: Topbar Toggler -->
                        </div>
                    </div>
                </div>

                <!-- END: Brand -->
                <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">

                    <!-- BEGIN: Horizontal Menu -->
                    <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark "
                            id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
                    <div id="m_header_menu"
                         class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-dark m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-dark m-aside-header-menu-mobile--submenu-skin-dark ">
                        <ul class="m-menu__nav ">

                            <li class="m-menu__item  m-menu__item--submenu m-menu__item--rel"
                                m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="true"><a
                                        href="javascript:;" class="m-menu__link m-menu__toggle"
                                        title="Non functional dummy link"><i
                                            class="m-menu__link-icon flaticon-line-graph"></i><span
                                            class="m-menu__link-text">Reportes</span><i
                                            class="m-menu__hor-arrow la la-angle-down"></i><i
                                            class="m-menu__ver-arrow la la-angle-right"></i></a>
                                <div class="m-menu__submenu  m-menu__submenu--fixed m-menu__submenu--left"
                                     style="width:1000px">
                                    <div class="m-menu__subnav">
                                        <ul class="m-menu__content">
                                            <li class="m-menu__item">
                                                <h3 class="m-menu__heading m-menu__toggle"><span
                                                            class="m-menu__link-text">Reportes</span><i
                                                            class="m-menu__ver-arrow la la-angle-right"></i></h3>
                                                <ul class="m-menu__inner">
                                                    <li class="m-menu__item " m-menu-link-redirect="1"
                                                        aria-haspopup="true"><a href="actions.html"
                                                                                class="m-menu__link "><i
                                                                    class="m-menu__link-icon flaticon-map"></i><span
                                                                    class="m-menu__link-text">Reportes Anuales</span></a>
                                                    </li>
                                                    <li class="m-menu__item " m-menu-link-redirect="1"
                                                        aria-haspopup="true"><a href="actions.html"
                                                                                class="m-menu__link "><i
                                                                    class="m-menu__link-icon flaticon-user"></i><span
                                                                    class="m-menu__link-text">HR Reports</span></a></li>
                                                    <li class="m-menu__item " m-menu-link-redirect="1"
                                                        aria-haspopup="true"><a href="actions.html"
                                                                                class="m-menu__link "><i
                                                                    class="m-menu__link-icon flaticon-clipboard"></i><span
                                                                    class="m-menu__link-text">IPO Reports</span></a>
                                                    </li>
                                                    <li class="m-menu__item " m-menu-link-redirect="1"
                                                        aria-haspopup="true"><a href="actions.html"
                                                                                class="m-menu__link "><i
                                                                    class="m-menu__link-icon flaticon-graphic-1"></i><span
                                                                    class="m-menu__link-text">Finance Margins</span></a>
                                                    </li>
                                                    <li class="m-menu__item " m-menu-link-redirect="1"
                                                        aria-haspopup="true"><a href="actions.html"
                                                                                class="m-menu__link "><i
                                                                    class="m-menu__link-icon flaticon-graphic-2"></i><span
                                                                    class="m-menu__link-text">Revenue Reports</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <!-- END: Horizontal Menu -->

                    <!-- BEGIN: Topbar -->
                    <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
                        <div class="m-stack__item m-topbar__nav-wrapper">
                            <ul class="m-topbar__nav m-nav m-nav--inline">
                                <li class="m-nav__item m-dropdown m-dropdown--large m-dropdown--arrow m-dropdown--align-center m-dropdown--mobile-full-width m-dropdown--skin-light	m-list-search m-list-search--skin-light"
                                    m-dropdown-toggle="click" id="m_quicksearch" m-quicksearch-mode="dropdown"
                                    m-dropdown-persistent="1">
                                    <a href="#" class="m-nav__link m-dropdown__toggle">
                                            <span class="m-nav__link-icon"><span class="m-nav__link-icon-wrapper"><i
                                                            class="flaticon-search-1"></i></span></span>
                                    </a>
                                    <div class="m-dropdown__wrapper">
                                        <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                        <div class="m-dropdown__inner ">
                                            <div class="m-dropdown__header">
                                                <form class="m-list-search__form">
                                                    <div class="m-list-search__form-wrapper">
                                                            <span class="m-list-search__form-input-wrapper">
                                                                <input id="m_quicksearch_input" autocomplete="off"
                                                                       type="text" name="q"
                                                                       class="m-list-search__form-input" value=""
                                                                       placeholder="Search...">
                                                            </span>
                                                        <span class="m-list-search__form-icon-close"
                                                              id="m_quicksearch_close">
                                                                <i class="la la-remove"></i>
                                                            </span>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="m-dropdown__body">
                                                <div class="m-dropdown__scrollable m-scrollable"
                                                     data-scrollable="true" data-height="300"
                                                     data-mobile-height="200">
                                                    <div class="m-dropdown__content">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="m-nav__item m-topbar__user-profile  m-dropdown m-dropdown--medium m-dropdown--arrow  m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light"
                                    m-dropdown-toggle="click">
                                    <a href="#" class="m-nav__link m-dropdown__toggle">
                                            <span class="m-topbar__userpic">
                                                <img src="assets/app/media/img/users/user4.jpg"
                                                     class="m--img-rounded m--marginless m--img-centered" alt="" />
                                            </span>
                                        <span class="m-nav__link-icon m-topbar__usericon  m--hide">
                                                <span class="m-nav__link-icon-wrapper"><i
                                                            class="flaticon-user-ok"></i></span>
                                            </span>
                                        <span class="m-topbar__username m--hide">Sam</span>
                                    </a>
                                    <div class="m-dropdown__wrapper">
                                            <span
                                                    class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                        <div class="m-dropdown__inner">
                                            <div class="m-dropdown__header m--align-center">
                                                <div class="m-card-user m-card-user--skin-light">
                                                    <div class="m-card-user__pic">
                                                        <img src="assets/app/media/img/users/user4.jpg"
                                                             class="m--img-rounded m--marginless" alt="" />
                                                    </div>
                                                    <div class="m-card-user__details">
                                                        <span class="m-card-user__name m--font-weight-500">Daniela Sanmiguel</span>
                                                        <a href=""
                                                           class="m-card-user__email m--font-weight-300 m-link">dasamo1010@gmail.com</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="m-dropdown__body">
                                                <div class="m-dropdown__content">
                                                    <ul class="m-nav m-nav--skin-light">
                                                        <li class="m-nav__section m--hide">
                                                            <span class="m-nav__section-text">Section</span>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <a href="profile.html" class="m-nav__link">
                                                                <i class="m-nav__link-icon flaticon-profile-1"></i>
                                                                <span class="m-nav__link-title">
                                                                        <span class="m-nav__link-wrap">
                                                                            <span class="m-nav__link-text">My
                                                                                Profile</span>
                                                                            <span class="m-nav__link-badge"><span
                                                                                        class="m-badge m-badge--success">2</span></span>
                                                                        </span>
                                                                    </span>
                                                            </a>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <a href="profile.html" class="m-nav__link">
                                                                <i class="m-nav__link-icon flaticon-share"></i>
                                                                <span class="m-nav__link-text">Activity</span>
                                                            </a>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <a href="profile.html" class="m-nav__link">
                                                                <i class="m-nav__link-icon flaticon-chat-1"></i>
                                                                <span class="m-nav__link-text">Messages</span>
                                                            </a>
                                                        </li>
                                                        <li class="m-nav__separator m-nav__separator--fit">
                                                        </li>

                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>

                    <!-- END: Topbar -->
                </div>
            </div>
        </div>
    </header>
    <!-- END: Header -->
    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        <!-- BEGIN: Left Aside -->
        <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i
                    class="la la-close"></i></button>
        <div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

            <!-- BEGIN: Aside Menu -->
            <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
                 m-menu-vertical="1" m-menu-scrollable="0" m-menu-dropdown-timeout="500">
                <ul class="m-menu__nav ">
                    <li class="m-menu__section m-menu__section--first">
                        <h4 class="m-menu__section-text">Departments</h4>
                        <i class="m-menu__section-icon flaticon-more-v2"></i>
                    </li>

                    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"
                        m-menu-submenu-toggle="hover"><a href="javascript:;"
                                                         class="m-menu__link m-menu__toggle"><span class="m-menu__item-here"></span><i
                                    class="m-menu__link-icon flaticon-layers"></i><span
                                    class="m-menu__link-text">Inventario</span><i
                                    class="m-menu__ver-arrow la la-angle-right"></i></a>
                        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true"><span
                                            class="m-menu__link"><span class="m-menu__item-here"></span><span
                                                class="m-menu__link-text">Resources</span></span></li>
                                <li class="m-menu__item " aria-haspopup="true"><a href="inner.html"
                                                                                  class="m-menu__link "><i
                                                class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span
                                                class="m-menu__link-text">Timesheet</span></a></li>
                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a
                                            href="inner.html" class="m-menu__link "><i
                                                class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span
                                                class="m-menu__link-text">Payroll</span></a></li>
                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a
                                            href="inner.html" class="m-menu__link "><i
                                                class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span
                                                class="m-menu__link-text">Contacts</span></a></li>
                            </ul>
                        </div>
                    </li>

                    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"
                        m-menu-submenu-toggle="hover" m-menu-link-redirect="1"><a href="javascript:;"
                                                                                  class="m-menu__link m-menu__toggle"><span class="m-menu__item-here"></span><i
                                    class="m-menu__link-icon flaticon-graphic-1"></i><span class="m-menu__link-title">
                                    <span class="m-menu__link-wrap"> <span class="m-menu__link-text">Ventas</span>
                                        <span class="m-menu__link-badge"><span
                                                    class="m-badge m-badge--accent">3</span></span> </span></span><i
                                    class="m-menu__ver-arrow la la-angle-right"></i></a>
                        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true"
                                    m-menu-link-redirect="1"><span class="m-menu__link"><span
                                                class="m-menu__item-here"></span><span class="m-menu__link-title"> <span
                                                    class="m-menu__link-wrap"> <span
                                                        class="m-menu__link-text">Support</span>
                                                    <span class="m-menu__link-badge"><span
                                                                class="m-badge m-badge--accent">3</span></span>
                                                </span></span></span></li>

                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a
                                            href="inner.html" class="m-menu__link "><i
                                                class="m-menu__link-bullet m-menu__link-bullet--line"><span></span></i><span
                                                class="m-menu__link-text">Clients</span></a></li>
                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a
                                            href="inner.html" class="m-menu__link "><i
                                                class="m-menu__link-bullet m-menu__link-bullet--line"><span></span></i><span
                                                class="m-menu__link-text">Audit</span></a></li>
                            </ul>
                        </div>
                    </li>

                    <li class="m-menu__section ">
                        <h4 class="m-menu__section-text">Reports</h4>
                        <i class="m-menu__section-icon flaticon-more-v2"></i>
                    </li>
                    <!--<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="inner.html"
                            class="m-menu__link "><span class="m-menu__item-here"></span><i
                                class="m-menu__link-icon flaticon-graphic"></i><span
                                class="m-menu__link-text">Accounting</span></a></li>-->
                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href=""
                                                                                               class="m-menu__link "><span class="m-menu__item-here"></span><i
                                    class="m-menu__link-icon flaticon-network"></i><span
                                    class="m-menu__link-text">Clientes</span></a></li>
                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="inner.html"
                                                                                               class="m-menu__link "><span class="m-menu__item-here"></span><i
                                    class="m-menu__link-icon flaticon-coins"></i><span
                                    class="m-menu__link-text">Fiado</span></a></li>
                </ul>
            </div>

            <!-- END: Aside Menu -->
        </div>
        <!-- END: Left Aside -->

        <!-- BEGIN: Right Content -->
        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <!-- BEGIN: Subheader -->
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title ">Agregar Clientes</h3>
                    </div>
                </div>
            </div>
            <!-- END: Subheader -->

            <div class="m-content">
                <!-- BEGIN: page content -->
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Modificar los datos del Producto
                                </h3>
                            </div>
                            <div class="m-portlet__head-tools">
                                <div style="float: right" class="m-portlet__nav">
                                    <input class="btn btn-success" type="button" value="Adicionar" id="adicionar" />
                                    <input class="btn btn-info" type="button" value="Modificar" id="modificar" />
                                    <input class="btn btn-danger" type="button" value="Act/Des" id="eliminar" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div id="m_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                            <div class="container-fluid">
                                <div class="clearfix"></div>
                                <table  id="example" class="display table table-striped responsive-utilities jambo_table">
                                    <thead>
                                    <tr class="headings">
                                        <th>Código</th>
                                        <th>Nombre</th>
                                        <th>Tipo</th>
                                        <th>Precio </th>
                                        <th>Iva</th>
                                        <th>Presentación</th>
                                        <th>Estado</th>
                                        <th>Fecha de Vencimiento</th>
                                    </tr>
                                    </thead>
                                </table>
                                <div id="dialogo-mensaje" title="Gestión de Productos">
                                </div>
                                <div id="dialog-form" title="Modificar tipo de huevo">
                                    <p class="validateTips">Todos los campos son requeridos.</p>
                                    <div class="form-row">
                                        <div class="col-md-4 mb-3">
                                            <label for="validationDefault01"><strong>Código Producto</strong></label>
                                            <input type="number" class="form-control" id="codigoProducto" placeholder="Código" name="txtcodigo"  required>
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="validationDefault01"><strong>Nombre Producto</strong></label>
                                            <input type="text" class="form-control" id="nombreProducto" placeholder="Nombre" name="txtnombre"  required>
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="validationDefault01"><strong>Tipo de Producto</strong></label>
                                            <input type="text" class="form-control" id="tipoProducto" placeholder="Tipo" name="txtTipo"  required>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-4 mb-3">
                                            <label for="validationDefault01"><strong>Precio Producto</strong></label>
                                            <input type="number" class="form-control" id="precioProducto" placeholder="Código" name="txtprecio"  required>
                                        </div>
                                        <div class="col-md-4 mb-3 ">
                                            <label for="validationDefault01"><strong>Iva</strong></label>
                                            <input type="number" class="form-control" id="precioProducto" placeholder="Código" name="txtprecio"  required>
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="validationDefault01"><strong>Presentación</strong></label>
                                            <input type="text" class="form-control" id="nombreProducto" placeholder="Nombre" name="txtnombre"  required>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-4 mb-3">
                                            <div class="form-group">
                                                <label class="control-label"><strong>Estado</strong></label>
                                                <div>
                                                    <select class="custom-select form-control " id="tipo-dropdown" id="tipo">
                                                        <option selected><strong>-Seleccionar Estado-</strong></option>
                                                        <option value="1"><strong>Activo</strong></option>
                                                        <option value="2"><strong>Inactivo</strong></option>
                                                    </select>
                                                    <input type="hidden" id=""
                                                           value="">
                                                    <input type="hidden" id=""
                                                           value="">
                                                    <input type="hidden" id=""
                                                           value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <div class="form-group">
                                                <label class="control-label ">Calendario</label>
                                                <div class='input-group date' id='datetimepicker1'>
                                                    <input type='text' id="fecha" class="form-control" />
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                        <i class="la la-calendar-check-o"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                            </div>
                    </div>
                </div>
                        <!-- END: page content -->
                    </div>

                </div>
                <!-- END: Right Content -->
            </div>
            <!-- end: Body -->
</div>
<!-- end: Page -->

<!--begin::Global Theme Bundle -->
<script src="/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
<script src="/assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

<!--end::Global Theme Bundle -->

<!--begin::Page Scripts -->
<script src="/assets/app/js/dashboard.js" type="text/javascript"></script>

<!--end::Page Scripts -->


</body>
<!-- end::Body -->
</html>