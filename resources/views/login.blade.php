<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Facturacion</title>

        <!-- Fonts -->
        
        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        
    </head>
    <body>
        <div class="container" id="app_login">
            <div class="col-md-4 offset-md-4 mt-5">
                <h1 class="my-3 text-center">Iniciar sesion</h1>
                <div class="card">
                    <div class="card-body">
                        <form id="formulario-login">
                            <div class="form-group">
                                <label for="usuario">Usuario</label>
                                <input type="email" v-model="usuario" class="form-control" id="usuario" placeholder="Ingrese Usuario" required>
                            </div>
                            <div class="form-group">
                                <label for="pass">Contraseña</label>
                                <input type="password" v-model="clave" class="form-control" id="pass" placeholder="Ingrese Contraseña" required>
                            </div>
                            <button type="button" class="btn btn-primary" @click="iniciarSesion()">Enviar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
